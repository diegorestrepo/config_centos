#!/bin/bash

# * ------------------------------------------------------------------------------------
# * "THE BEER-WARE LICENSE" (Revision 42):
# * <diegorestrepoleal@gmail.com> wrote this file. As long as you retain this notice you
# * can do whatever you want with this stuff. If we meet some day, and you think
# * this stuff is worth it, you can buy me a beer in return Diego Andrés Restrepo Leal.
# * ------------------------------------------------------------------------------------

# Limpiar pantalla
clear

# Color de los mensajes
AZUL=$(tput setaf 6)
VERDE=$(tput setaf 2)
ROJO=$(tput setaf 1)
LILA=$(tput setaf 5)
LIMPIAR=$(tput sgr 0)

echo
echo "$AZUL Instalar wget $LIMPIAR"
echo
yum install -y wget

echo
echo "$AZUL Instalar perl $LIMPIAR"
echo
yum install -y perl
perl -pi -e "s/enabled=0/enabled=1/" /etc/yum.repos.d/CentOS-Base.repo

echo
echo "$AZUL Instalar rsync $LIMPIAR"
echo
yum install -y rsync

echo
echo "$AZUL Instalar gcc $LIMPIAR"
echo
yum install -y gcc

echo
echo "$AZUL Instalar gfortran $LIMPIAR"
echo
yum install -y gcc-gfortran

echo
echo "$AZUL Instalar BLAS $LIMPIAR"
echo
yum install -y blas blas-devel

echo
echo "$AZUL Instalar LAPACK $LIMPIAR"
echo
yum install -y lapack lapack-devel

echo
echo "$AZUL Intalar fftw $LIMPIAR"
echo
yum install -y fftw fftw-devel

echo
echo "$AZUL Instalar nano $LIMPIAR"
echo
yum install -y nano

echo
echo "$AZUL Instalar tmux $LIMPIAR"
echo
yum install -y tmux

echo
echo "$AZUL Instalar Apache $LIMPIAR"
echo
rpm -Uvh https://dl.fedoraproject.org/pub/epel/epel-release-latest-7.noarch.rpm
rpm -Uvh http://rpms.famillecollet.com/enterprise/remi-release-7.rpm
yum --enablerepo=remi,remi-php72 -y install httpd php php-common
yum --enablerepo=remi,remi-php72 -y install php-pecl-apcu php-cli php-pear php-pdo php-mysqlnd php-pgsql php-pecl-mongodb php-pecl-memcache php-pecl-memcached php-gd php-mbstring php-mcrypt php-xml
systemctl start httpd.service
systemctl enable httpd.service
firewall-cmd --get-active-zones
firewall-cmd --permanent --zone=public --add-service=http
firewall-cmd --permanent --zone=public --add-port=80/tcp
systemctl restart firewalld.service

echo
echo "$AZUL Instalar MariaDB $LIMPIAR"
echo
yum install -y mariadb mariadb-server
systemctl start mariadb.service
systemctl enable mariadb.service
/usr/bin/mysql_secure_installation

echo
echo "$AZUL Deshabilitar $LIMPIAR"
echo
perl -pi -e "s/enabled=1/enabled=0/" /etc/yum.repos.d/CentOS-Base.repo

exit 0
